**Title**: CICD Enablement Workshop Repository

**Description**:

Welcome to the CICD Enablement Workshop Repository on GitLab! This repository is designed to provide sample scenarios and resources to help team present and demonstrate  Continuous Integration/Continuous Deployment (CICD) practices effectively. Whether the attendees is new to CICD or looking to enhance his skills, this repository has you covered.

In this project, you'll find a collection of sample scenarios in the issues, each with its own connected merge request (MR) to demonstrate best practices and workflows. These scenarios cover a range of CICD functions, including parent child, inheritance, extension, multiproject, variables precedence, testing, and deploying applications.

Additionally, this repository includes extra CICD functions that you can integrate into your workflows to further show other cicd functions

To get started, it is recommended to use this [PSE Enablement Deck](https://docs.google.com/presentation/d/1Ox8CufPOTZlLq9ifb-E7bNjNBV4FrBhwx5tAx2lyoeo/edit?usp=sharing), which offers materials that cover most of the topics required to pass the PSE certificafitions. You can refer to the issues in this project to showcase samples for each CICD scenario discussed in the workshop.

It's important to note that some scenarios, such as multiproject and parent-child setups, require this [child project](https://gitlab.com/sakkoub-group/cicd-workshop/project-templates/child-project) to be available alongside this sample project. Ensure you have access to these projects to fully explore the capabilities of CICD.

For certain functionalities like deploying Node.js applications and conducting DAST security scanning, make sure to add these    necessary environment variables for seamless integration.

KUBE_INGRESS_BASE_DOMAIN: $VAR_KUBE_INGRESS_BASE_DOMAIN 

KUBE_CONTEXT: $VAR_KUBE_CONTEXT # Define a gitlab agent and pass the context to this group



To streamline your workshop experience, we recommend importing this project and its associated child projects into your project templates group. From there, you can easily create new projects within your group using these project templates, facilitating seamless workshop setup and execution.